# Course notes for 4640 Winter 2024

## Weekly Notes

[Week 1](4640_Notes/Week1.md)

- Introductions
- Dev environment
  - [WSL Supplement](4640_Notes/Week1_wsl_supplement.md)

[Week 2](4640_Notes/Week2.md)

- Linux review
  - Shell scripting
  - `systemctl` commands
- AWS CLI review

[Week 3](4640_Notes/Week3.md)

- AWS CLI Scripting Review
  - [Week 2 In-Class Activity Solution](/solutions/week02_inclass/readme.md)
- SSH Review
  - [SSH Keys](4640_Notes/Week3_ssh_keys.md)
- Terraform Intro

[Week 4](4640_Notes/Week4.md)

- Week 3 In-Class Activity Solution:
  - [main.tf](/solutions/week03_inclass/),
  - [ec2_host_setup.sh](/solutions/week03_inclass/ec2_host_setup.sh)

[Week 5](4640_Notes/Week5.md)

- Intro to Ansible
- ad hoc commands
- playbooks
- [Ansible Notes](subject_notes/ansible_notes.md)
- [Terraform Starter Code](starter_code/week05_vpc/main.tf)

[Week 6](4640_Notes/Week6.md)

- Terraform state
  - [Terraform State Exploration](starter_code/week06_state_exploration/)
- Terraform modules

## Assignments

[Assignment 1](../assignments/Assignment-1.md)

- Due Friday February 16

## References

See [resources.md](4640_Notes/resources.md) for a list of references.
